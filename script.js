'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const SumInterestlabel = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');


const displayMovements = function (movements) {

  console.log(containerMovements.innerHTML);

  containerMovements.innerHTML = '';
  movements.forEach(function (mov, i) {

    const type = mov > 0 ? "deposit" : "withdrawal";
    const html = `
    <div class="movements__row">
          <div class="movements__type movements__type--${type}">${i + 1} ${type}</div>
          <div class="movements__date">3 days ago</div>
          <div class="movements__value">${mov}</div>
    </div>`

    containerMovements.insertAdjacentHTML('afterbegin', html);
  })



}

const create_user_name = function (accs) {
  accs.forEach(function (acc) {
    acc.username = acc.owner.toLowerCase()
      .split(" ")
      .map(name => name[0]).join("");
  })
}

create_user_name(accounts);

const calc_balance_summery = function (acc) {
  const balance = acc.movements.reduce((accu, arr) => accu + arr, 0);

  labelBalance.textContent = `${balance} EURO`;

  const deposit = acc.movements.filter(mov => mov > 0)
    .reduce((accu, curr) => accu + curr, 0);


  labelSumIn.textContent = `${deposit} 💷`;

  const interest = acc.movements.filter(moving => moving > 0)
    .map((mov,) => mov * (acc.interestRate / 100))
    .reduce((accu, curr) => accu + curr, 0);

  SumInterestlabel.textContent = `${interest}💷`;


}








displayMovements(account1.movements);

let currentAccount;

btnLogin.addEventListener('click', function (e) {

  e.preventDefault();
  console.log('login');

  currentAccount = accounts.find(acc => acc.username === inputLoginUsername.value);
  console.log(currentAccount);

  if (currentAccount?.pin === Number(inputLoginPin.value)) {


    labelWelcome.textContent = `Welcome To Your Account , ${currentAccount.owner}`;

    containerApp.style.opacity = 100;


    inputLoginUsername.value = inputLoginPin.value = "";

    //Display balance

    calc_balance_summery(currentAccount);



  }
})

console.log(accounts);



/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

/////////////////////////////////////////////////


// THE MAP METHOD
// ================

// const multyply = 2;

// console.log(movements.map(function (mov) {
//   return mov * multyply;
// }));////===> another way of modern method of this function is just showed below
// ///what is called arrow function -----

// console.log(movements.map(mov => mov * 4));


// ///What I understand from """map method""" that it create another array with its parent array total 
// ///amount , it needs a function to deliver the parents array'e element;

// ///---------------Now I can see another example like before------
// ///---------------But in a different way ------------------------

// const multyply_store = []; //that's the array where we store new produce array

// for (const mov of movements) multyply_store.push(mov * 3);//Here we use the loop ""for""
// //for iterating the parents array , and the variable ''mov'' keeps the array element ,push 
// //it to the previous defined array ''multyply_store'' array



// console.log(multyply_store);

// const movementsInDetails = movements.map((mov, i, arr) => {
//   if (mov > 0)
//     console.log(`${ i + 1 } number is depsoited, the amount is  ${ mov } `);
//   else
//     console.log(`${ i + 1 } number is withdraw, the amount is ${ mov } `);
// }
// )

// console.log(movementsInDetails);


// const username = "Syed Ahmmed Bondhu";

// const user = username.toLowerCase().split(" ").map(name => { return name[0] }).join(''); //the split method here takes the string into
// // an array and ''the qoutes with a spce takes whole word''' and  ''the quotes without space ''
// //only takes the alphabet.

// // arrow function does not need _______return___________ 

// // the three things are going in here - 1.split method takes three word in an array respectively
// //2. the map method takes that array only return the first element(here is the first 
// // alphabet ) and then the join method with single qoutes join those aphabet and make it 
// // into a string 

// console.log(user);


///////////////////////////////NOW THE WHOLE WE NEED TO ADD INTO A FUNCTION

// const create_user_name = function (user_name) {
//   return user_name.toLowerCase()
//     .split(" ")
//     .map(name => { return name[0] }).join('');
// }



// console.log(create_user_name('Nafisa Islam Nirva'));


/////I try to make this code works as a username genarator of our previous declared object

// const create_user_name = function (accs) {
//   accs.forEach(function (acc) {
//     acc.username = acc.owner.toLowerCase()
//       .split(" ")
//       .map(name => name[0]).join('');
//   })
// }

// create_user_name(accounts);

// console.log(accounts);


////////////////FILTER METHOD////////////////////////////

const deposit = movements.filter(function (mov) {
  return mov > 0;
});

console.log(deposit);

const withdraw = movements.filter(function (mov) {
  return mov < 0;
});

console.log(withdraw);

//=============== REDUCE METHOD ===============



// const balance = movements.reduce(function (accu, curr,i , arr) {
//   console.log(`${ i + 1 } iteration and value is: ${ accu }, and array element is ${ curr } `);
//   return accu + curr;
// }, 0);

// console.log(balance);

////same function with more organized way

const balance = movements.reduce((accu, curr) => accu + curr, 0);

console.log(balance);

//MAXIMUM VALUE 

const muximum_value = movements.reduce((accu, curr) => {
  if (accu > curr) return accu;
  else return curr;
}, movements[0]);

console.log(muximum_value);

///////////////////////////////////////
// Coding Challenge #2

/*
Let's go back to Julia and Kate's study about dogs. This time, they want to convert dog ages to human ages and calculate the average age of the dogs in their study.

Create a function 'calcAverageHumanAge', which accepts an arrays of dog's ages ('ages'), and does the following things in order:

1. Calculate the dog age in human years using the following formula: if the dog is <= 2 years old, humanAge = 2 * dogAge. If the dog is > 2 years old, humanAge = 16 + dogAge * 4.
2. Exclude all dogs that are less than 18 human years old (which is the same as keeping dogs that are at least 18 years old)
3. Calculate the average human age of all adult dogs (you should already know from other challenges how we calculate averages 😉)
4. Run the function for both test datasets

TEST DATA 1: [5, 2, 4, 1, 15, 8, 3]
TEST DATA 2: [16, 6, 10, 5, 6, 1, 4]

GOOD LUCK 😀

*/

// const calcAverageHumanAge = function (ages) {
//   const humanAge = ages.map(age => (age <= 2 ? 2 * age : 16 + age * 4));

// }

// const age = [5, 2, 4, 1, 15, 8, 3];

// console.log(calcAverageHumanAge(age));


const euroToUsd = 1.1;

const totalDepositUsd = movements
  .filter(mov => mov > 0)
  .map((mov, i, arr) => {
    console.log(arr);

    return mov * euroToUsd;
  })
  .reduce((accu, curr) => accu + curr, 0);

console.log(totalDepositUsd);


//===================The FIND METHOD===================

const withdraw_at_first = movements.find(mov => mov < 0);

console.log(withdraw_at_first);

// the find method needs a logic the find it desired element
// It also works in object 

console.log(accounts);


const finding = accounts.find(acc => acc.pin === 1111);

console.log(finding);






